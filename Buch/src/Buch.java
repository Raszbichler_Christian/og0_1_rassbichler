
public class Buch implements Comparable<Buch> {
	private String autor;
	private String buchname;
	private String nummer;

	public Buch(String autor, String buchname, String nummer) {
		// TODO Auto-generated constructor stub
	}

	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getBuchname() {
		return buchname;
	}
	public void setBuchname(String buchname) {
		this.buchname = buchname;
	}
	public String getNummer() {
		return nummer;
	}
	public void setNummer(String nummer) {
		this.nummer = nummer;
	}

	@Override
	public int compareTo(Buch o) {
		
		return this.nummer.compareTo(nummer);
	}

	
	

}
