package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);

		if (obj == f.btnEinfuellen) {
			double fuellstand = f.myTank.getFuellstand();
			if(fuellstand == 100) {
				fuellstand = fuellstand +0;}
			else {
				fuellstand = fuellstand + 5;
			}
			f.myTank.setFuellstand(fuellstand);

			f.lblFuellstand.setText("" + fuellstand +" das entspricht " +fuellstand +"%");
		}
		if (obj == f.btnVerbrauchen) {

			double fuellstand = f.myTank.getFuellstand();
		
			if(fuellstand <= 1) {
				fuellstand =  0 ;}
			else {
				fuellstand = fuellstand - 2;
			}
			f.myTank.setFuellstand(fuellstand);
			f.lblFuellstand.setText("" + fuellstand +" das entspricht " +fuellstand + "%") ;
			
		}
		if (obj == f.btnZuruecksetzen) {

			double fuellstand = f.myTank.getFuellstand();
			fuellstand = 0;
			f.myTank.setFuellstand(fuellstand);
			f.lblFuellstand.setText("" + fuellstand +" das entspricht " +fuellstand +"%");
		}
	}
}