package de.oszimt.starsim2099;

public class RaumfartDings {

	private double posX;
	private double posY;
	
	public RaumfartDings(double x, double y) {
		
		this.posX= x;
		this.posY=y;
	}
	public void setposX(double x) {
		posX = x ;
	}
	public double getposX() {
		return posX;
	}
	public void setposY(double y) {
		posY = y ;
	}
	public double getposY() {
		return posY;
	}
}
