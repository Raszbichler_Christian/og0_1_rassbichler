import java.util.ArrayList;

public class KeyStore01 {
	private String[] al;
	private int pos;
	public static final int Maxpos = 100;
	public final int Nichtgefunden = 0;

	public KeyStore01() {
		this.al = new String[100];
		this.pos = 0;
	}

	public KeyStore01(int length) {
		this.al = new String[length];
		this.pos = 0;
	}

	public boolean add(java.lang.String eintrag) {
		if (pos < Maxpos) {
			this.al[this.pos] = eintrag;
			this.pos++;
			return true;
		} else
			return false;
	}

	public String toString() {
		return "KeyStore [toString()=]" + super.toString() + "]";

	}

	public int indexOf(java.lang.String eintrag) {
		//lineare Suche
		for(int i = 0; i < this.pos ; i++) {
			if(this.al[i].equals(eintrag));{ 
				return i;
				}
		}
		return Nichtgefunden;
	}

	public void remove(int index) {
		if(index > 0 && index <= pos) {
			for(int i = index; i< pos; i++) {
				this.al[i] = this.al[i +1];
			}
			pos--;
		}
	}

}
