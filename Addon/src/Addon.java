
public class Addon {

	private int ID_Nummer;
	private String Bezeichnung;
	private double Verkaufspreis;
	private int anzahl;
	private String art;

	public Addon(int ID_Nummer, String Bezeichnung, double Verkaufspreis, int anzahl, String art) {

		ID_Nummer = this.ID_Nummer;
		Bezeichnung = this.Bezeichnung;
		Verkaufspreis = this.Verkaufspreis;
		anzahl = this.anzahl;
		art = this.art;
	}

	public void setID_Nummer(int ID_Nummer) {
		this.ID_Nummer = ID_Nummer;
	}

	public int getID_Nummer() {
		return ID_Nummer;
	}

	public void setBezeichnung(String Bezeichnung) {
		this.Bezeichnung = Bezeichnung;
	}

	public String getbezeichnung() {
		return Bezeichnung;
	}

	public void setVerkaufspreis(double Verkaufspreis) {
		this.Verkaufspreis = Verkaufspreis;
	}

	public double getVerkaufspreis() {
		return Verkaufspreis;
	}

	public void setanzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	public int anzahl() {
		return anzahl;
	}

	public void setart(String art) {
		this.art = art;
	}

	public String getart() {
		return art;
	}

	public void aendereBestnad(int aendereBestnad) {
		this.anzahl = aendereBestnad;

		
	}

	public void gesamtWert(double gesamtWert) {
		gesamtWert =anzahl*Verkaufspreis;
	}
}
